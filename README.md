# Instruksi Instalasi Proyek Rental Mobil

Berikut adalah langkah-langkah untuk menginstal dan menjalankan proyek Rental Mobil:

## Persyaratan Pra-Instalasi
Pastikan Anda telah memenuhi persyaratan berikut sebelum menginstal proyek Rental Mobil:
- pip 8.1+
- MySQL

## Langkah-langkah Instalasi

1. **Klon Proyek:**
   Klon proyek dari repositori GitLab dengan menjalankan perintah berikut di terminal atau command prompt:

   ```bash
   git clone https://gitlab.com/sefi06/rental-mobil.git

2. **Composer Install:**
   jalankan composer install

   ```bash
   composer install

3. **Sesuaikan Environment:**
   Sesuaikan file .env untuk konfigurasi database dan APP_NAME:

4. **Migrasi:**
   Jalankan file Migration:

   ```bash
   php artisan migrate

5. **Let's Start:**
   Jalankan artisan serve:

   ```bash
   php artisan serve