<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstCar extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function owner() {
        return $this->hasOne(User::class, 'id', 'iduser');
    }
}
