<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrsBookingReturn extends Model
{
    use HasFactory;

    protected $table = 'trs_bookings';

    protected $guarded = ['id'];

    public function mstcar() {
        return $this->hasOne(MstCar::class, 'id', 'idcar');
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'iduser');
    }

}
