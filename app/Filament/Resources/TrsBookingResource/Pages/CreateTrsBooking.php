<?php

namespace App\Filament\Resources\TrsBookingResource\Pages;

use App\Filament\Resources\TrsBookingResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTrsBooking extends CreateRecord
{
    protected static string $resource = TrsBookingResource::class;
}
