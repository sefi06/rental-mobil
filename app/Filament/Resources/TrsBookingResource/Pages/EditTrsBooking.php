<?php

namespace App\Filament\Resources\TrsBookingResource\Pages;

use App\Filament\Resources\TrsBookingResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTrsBooking extends EditRecord
{
    protected static string $resource = TrsBookingResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
