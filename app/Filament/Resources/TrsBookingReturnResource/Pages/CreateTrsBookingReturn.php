<?php

namespace App\Filament\Resources\TrsBookingReturnResource\Pages;

use App\Filament\Resources\TrsBookingReturnResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTrsBookingReturn extends CreateRecord
{
    protected static string $resource = TrsBookingReturnResource::class;
}
