<?php

namespace App\Filament\Resources\TrsBookingReturnResource\Pages;

use App\Filament\Resources\TrsBookingReturnResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTrsBookingReturn extends EditRecord
{
    protected static string $resource = TrsBookingReturnResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
