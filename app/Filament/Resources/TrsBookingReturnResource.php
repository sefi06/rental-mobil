<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TrsBookingReturnResource\Pages;
use App\Filament\Resources\TrsBookingReturnResource\RelationManagers;
use App\Models\MstCar;
use App\Models\TrsBookingReturn;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrsBookingReturnResource extends Resource
{
    protected static ?string $model = TrsBookingReturn::class;

    protected static ?string $navigationIcon    = 'heroicon-o-rectangle-stack';
    protected static ?string $navigationLabel   = 'Pengembalian Mobil';
    protected static ?string $label             = 'Pengembalian Mobil';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
            ]);
    }

    public static function table(Table $table): Table
    {
        if(auth()->user()->role == 'perental') {
            $status = TextColumn::make('status')->searchable()
                        ->getStateUsing(fn (MstCar $record): string => $record->status == 0 ? 'Disewa' : 'Tersedia')
                        ->badge()
                        ->color(fn (string $state): string => match ($state) {
                            'Disewa' => 'warning',
                            'Tersedia' => 'success',
                        })
                        ;
        }
        else {
            $status = TextColumn::make('status')->searchable()
                        ->getStateUsing(fn (MstCar $record): string => $record->status == 1 ? 'Dikembalikan' : 'Disewa')
                        ->badge()
                        ->color(fn (string $state): string => match ($state) {
                            'Disewa' => 'warning',
                            'Dikembalikan' => 'success',
                        });
        }

        return $table
            ->columns([
                TextColumn::make('merek')
                    ->searchable()
                    ->label('Merek'),
                TextColumn::make('model')
                    ->searchable()
                    ->label('Model'),
                TextColumn::make('plat_number')
                    ->searchable()
                    ->label('Nomor Plat'),
                TextColumn::make('startdate')
                    ->searchable()
                    ->label('Tanggal Sewa'),
                TextColumn::make('enddate')
                    ->searchable()
                    ->label('Tanggal Pengembalian'),
                TextColumn::make('qty')
                    ->searchable()
                    ->label('Jumlah Hari'),
                TextColumn::make('price')
                    ->searchable()
                    ->label('Total Harga'),
                $status
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\Action::make('edit')
                    ->label('Kembalikan')
                    ->form([

                    ])
                    ->modalHeading('Konfirmasi')
                    ->modalDescription('Apakah anda yakin ingin mengembalikan mobil yang anda sewa')
                    ->visible(fn (MstCar $record, Request $request): bool => $request->user()->role == 'peminjam')
                    ->action(function (array $data, MstCar $record): void {
                        $trs   = TrsBookingReturn::find($record->max_id);
                        
                        $trs->status    = 1;
                        $trs->save();

                        Notification::make()
                        ->title('Mobil Berhasil dikembalikan')
                        ->success()
                        ->send();
                    })
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([

                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        if(auth()->user()->role == 'perental') {
            // $query = MstCar::select('mst_cars.*')
            //         ->leftJoinSub(
            //             TrsBookingReturn::select('idcar', DB::raw('MAX(id) AS max_id'))
            //                 ->groupBy('idcar'),
            //             'latest_bookings',
            //             function ($join) {
            //                 $join->on('mst_cars.id', '=', 'latest_bookings.idcar');
            //             }
            //         )
            //         ->leftJoin('trs_bookings', function ($join) {
            //             $join->on('latest_bookings.idcar', '=', 'trs_bookings.idcar')
            //                 ->on('latest_bookings.max_id', '=', 'trs_bookings.id');
            //         })
            //         ->with('owner')
            //         ->where('mst_cars.iduser', auth()->id())
            //         ->where('status', 0);
            //         ;

            $query = MstCar::query()
                    ->select('mst_cars.*', 'trs.*')
                    ->leftJoinSub(
                        TrsBookingReturn::select('idcar', 'status', 'startdate', 'enddate', 'qty', DB::raw('MAX(id) AS max_id'))
                            ->groupBy('idcar', 'status', 'startdate', 'enddate', 'qty'),
                        'trs',
                        function ($join) {
                            $join->on('mst_cars.id', '=', 'trs.idcar');
                        }
                    )
                    ->with('owner')
                    ->where('mst_cars.iduser', auth()->id())
                    ->where('status', 0);
        }
        else {
            $query = MstCar::query()
                    ->select('mst_cars.*', 'trs.*')
                    ->leftJoinSub(
                        TrsBookingReturn::select('idcar', 'status', 'startdate', 'enddate', 'qty', DB::raw('MAX(id) AS max_id'))
                            ->addSelect([DB::raw('iduser as idpeminjam')])
                            ->groupBy('idcar', 'status', 'startdate', 'enddate', 'qty', 'idpeminjam'),
                        'trs',
                        function ($join) {
                            $join->on('mst_cars.id', '=', 'trs.idcar');
                        }
                    )
                    ->with('owner')
                    ->where('trs.idpeminjam', auth()->id())
                    ->where('status', 0);
        }
        
        return $query;
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTrsBookingReturns::route('/'),
        ];
    }
}
