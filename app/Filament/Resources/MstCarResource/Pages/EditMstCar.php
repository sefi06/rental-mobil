<?php

namespace App\Filament\Resources\MstCarResource\Pages;

use App\Filament\Resources\MstCarResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMstCar extends EditRecord
{
    protected static string $resource = MstCarResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
