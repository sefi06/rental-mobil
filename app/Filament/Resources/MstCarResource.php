<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MstCarResource\Pages;
use App\Filament\Resources\MstCarResource\RelationManagers;
use App\Models\MstCar;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class MstCarResource extends Resource
{
    protected static ?string $model = MstCar::class;

    protected static ?string $navigationIcon    = 'heroicon-o-rectangle-stack';
    protected static ?string $navigationLabel   = 'Data Mobil';
    protected static ?string $label             = 'Data Mobil';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('merek'),
                TextInput::make('model'),
                TextInput::make('plat_number')
                    ->label('Nomor Plat')
                    ->unique(),
                TextInput::make('price')
                    ->numeric()
                    ->label('Harga Sewa / Hari')
                    ->prefix('Rp')
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('merek')->searchable(),
                TextColumn::make('model')->searchable(),
                TextColumn::make('plat_number')->searchable(),
                TextColumn::make('price')->searchable(),
                TextColumn::make('iduser')->searchable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([

                ]),
            ]);
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->where('iduser', auth()->id());
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMstCars::route('/'),
            'create' => Pages\CreateMstCar::route('/create'),
            'edit' => Pages\EditMstCar::route('/{record}/edit'),
        ];
    }

    public static function canViewAny(): bool
    {
        return auth()->user()->role == 'perental';
    }
}
