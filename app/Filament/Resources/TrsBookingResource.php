<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TrsBookingResource\Pages;
use App\Filament\Resources\TrsBookingResource\RelationManagers;
use App\Models\MstCar;
use App\Models\TrsBooking;
use DateTime;
use Filament\Forms;
use Filament\Forms\Components\Actions\Action;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Facades\DB;

class TrsBookingResource extends Resource
{
    protected static ?string $model = TrsBooking::class;

    protected static ?string $navigationIcon    = 'heroicon-o-rectangle-stack';
    protected static ?string $navigationLabel   = 'Peminjaman Mobil';
    protected static ?string $label             = 'Peminjaman Mobil';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('merek')->searchable(),
                TextColumn::make('model')->searchable(),
                TextColumn::make('plat_number')
                    ->searchable()
                    ->label('Nomor Plat'),
                TextColumn::make('price')
                    ->searchable()
                    ->label('Harga / Hari'),
                TextColumn::make('owner.name')->searchable(),
                TextColumn::make('owner.phone_number')
                    ->searchable()
                    ->label('No HP'),
                TextColumn::make('status')->searchable()
                    ->getStateUsing(fn (MstCar $record): string => $record->status == 1 || $record->status == null ? 'Tersedia' : 'Disewa')
                    ->badge()
                    ->color(fn (string $state): string => match ($state) {
                        'Disewa' => 'warning',
                        'Tersedia' => 'success',
                    })
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\Action::make('sewa')
                    ->label('Sewa')
                    ->form([
                        DatePicker::make('startdate')->date()->required(),
                        DatePicker::make('enddate')->date()->required(),
                    ])
                    ->action(function (array $data, MstCar $record): void {
                        $trs   = new TrsBooking();
                        
                        $trs->idcar    = $record->id;
                        $trs->iduser   = auth()->id();
                        $trs->startdate= $data['startdate'];
                        $trs->enddate  = $data['enddate'];

                        $date1 = new DateTime($trs->startdate);
                        $date2 = new DateTime($trs->enddate);

                        $interval = $date1->diff($date2);

                        $trs->qty      = $interval->days + 1;
                        $trs->price    = $record->price * $trs->qty;

                        $trs->save();

                        Notification::make()
                        ->title('Berhasil disewa')
                        ->success()
                        ->send();
                    })
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getEloquentQuery(): Builder
    {
        $query = MstCar::select('mst_cars.*', 'trs_bookings.status')
                ->leftJoinSub(
                    TrsBooking::select('idcar', DB::raw('MAX(id) AS max_id'))
                        ->groupBy('idcar'),
                    'latest_bookings',
                    function ($join) {
                        $join->on('mst_cars.id', '=', 'latest_bookings.idcar');
                    }
                )
                ->leftJoin('trs_bookings', function ($join) {
                    $join->on('latest_bookings.idcar', '=', 'trs_bookings.idcar')
                        ->on('latest_bookings.max_id', '=', 'trs_bookings.id');
                })
                ->with('owner')
                ->where('status', '!=', 0)
                ->orWhereNull('status');

        return $query;
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTrsBookings::route('/'),
        ];
    }

    public static function canViewAny(): bool
    {
        return auth()->user()->role == 'peminjam';
    }
}
