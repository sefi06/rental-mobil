<?php

namespace App\Filament\Widgets;

use App\Models\MstCar;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;
use App\Models\TrsBooking;
use App\Models\TrsBookingReturn;
use Illuminate\Support\Facades\DB;

class StatsOverview extends BaseWidget
{
    protected function getStats(): array
    {
        if(auth()->user()->role == 'perental') {
            return [
                Stat::make('Total Mobil', 
                    MstCar::where('iduser', auth()->id())->count()
                ),
                Stat::make('Total Mobil Disewa', 
                    MstCar::query()
                    ->select('mst_cars.*', 'trs.*')
                    ->leftJoinSub(
                        TrsBookingReturn::select('idcar', 'status', 'startdate', 'enddate', 'qty', DB::raw('MAX(id) AS max_id'))
                            ->addSelect([DB::raw('iduser as idpeminjam')])
                            ->groupBy('idcar', 'status', 'startdate', 'enddate', 'qty', 'idpeminjam'),
                        'trs',
                        function ($join) {
                            $join->on('mst_cars.id', '=', 'trs.idcar');
                        }
                    )
                    ->with('owner')
                    ->where('status', 0)
                    ->count()
                ),
            ];
        }
        else {
            return [
                Stat::make('Total sewa Mobil', 
                    MstCar::query()
                    ->select('mst_cars.*', 'trs.*')
                    ->leftJoinSub(
                        TrsBookingReturn::select('idcar', 'status', 'startdate', 'enddate', 'qty', DB::raw('MAX(id) AS max_id'))
                            ->addSelect([DB::raw('iduser as idpeminjam')])
                            ->groupBy('idcar', 'status', 'startdate', 'enddate', 'qty', 'idpeminjam'),
                        'trs',
                        function ($join) {
                            $join->on('mst_cars.id', '=', 'trs.idcar');
                        }
                    )
                    ->with('owner')
                    ->where('trs.idpeminjam', auth()->id())
                    ->where('status', 0)
                    ->count()
                ),
                Stat::make('Mobil Tersedia', 
                    MstCar::select('mst_cars.*')
                    ->leftJoinSub(
                        TrsBooking::select('idcar', DB::raw('MAX(id) AS max_id'))
                            ->groupBy('idcar'),
                        'latest_bookings',
                        function ($join) {
                            $join->on('mst_cars.id', '=', 'latest_bookings.idcar');
                        }
                    )
                    ->leftJoin('trs_bookings', function ($join) {
                        $join->on('latest_bookings.idcar', '=', 'trs_bookings.idcar')
                            ->on('latest_bookings.max_id', '=', 'trs_bookings.id');
                    })
                    ->with('owner')
                    ->where('status', '!=', 0)
                    ->orWhereNull('status')
                    ->count()
                ),
            ];
        }
    }

    protected function getColumns(): int
    {
        return 2;
    }
}
