<?php

namespace App\Filament\Pages\Auth;

use Filament\Actions\Action;
use Filament\Pages\Auth\Register as BaseRegister;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\Component;

class Register extends BaseRegister
{
    protected function getForms(): array
    {
        return [
            'form' => $this->form(
                $this->makeForm()
                    ->schema([
                        $this->getNameFormComponent()->label('Nama Lengkap'),
                        $this->getEmailFormComponent()->label('Email'),
                        $this->getPasswordFormComponent(),
                        $this->getPasswordConfirmationFormComponent(),
                        $this->getPhoneFormComponent(),
                        $this->getSimFormComponent(),
                        $this->getAddressFormComponent(),
                        $this->getRoleFormComponent(), 
                    ])
                    ->statePath('data'),
            ),
        ];
    }

    protected function getRoleFormComponent(): Component
    {
        return Select::make('role')
                ->options([
                    'peminjam' => 'Peminjam Mobil',
                    'perental' => 'Perental Mobil',
                ])
                ->label('Mendaftar Sebagai')
                ->default('peminjam')
                ->required();
    }

    protected function getPhoneFormComponent(): Component
    {
        return TextInput::make('phone_number')
                ->label('Nomor HP')
                ->numeric()
                ->unique()
                ->required();
    }

    protected function getSimFormComponent(): Component
    {
        return TextInput::make('sim')
                ->label('Nomor SIM')
                ->numeric()
                ->required();
    }

    protected function getAddressFormComponent(): Component
    {
        return TextArea::make('address')
                ->label('Alamat')
                ->required();
    }
}
